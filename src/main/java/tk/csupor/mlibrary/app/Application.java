package tk.csupor.mlibrary.app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import tk.csupor.mlibrary.library.Library;
import tk.csupor.mlibrary.library.LibraryBook;
import tk.csupor.mlibrary.library.LocalLibrary;

public final class Application {

    private static boolean isApplicationRunning;
    private static MenuType menuType;
    private static BufferedReader bufferedReader;
    private static boolean renderErrorMsg;
    private static Library library;
    private static LibraryBook selectedBookToBorrow;
    private static boolean isBorrowSuccess;

    static {
        isApplicationRunning = true;
        menuType = MenuType.MAIN;
        bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        renderErrorMsg = false;
        library = new LocalLibrary();
        selectedBookToBorrow = null;
        isBorrowSuccess = false;
    }

    public static void run() {
        while (isApplicationRunning) {
            renderMenu(renderErrorMsg);

            try {
                String answer = bufferedReader.readLine();
                handleUserInput(answer);
            } catch (IOException e) {
                // Handle error silently
            } catch (InvalidUserInputException e) {
                renderErrorMsg = true;
            } catch (Exception e) {
                // Handle error silently
            }
        }
    }

    private static void handleUserInput(String answer) throws InvalidUserInputException {
        switch (menuType) {
            case MAIN:
                handleMainMenuUserInput(answer);
                break;
            case BORROW_MAIN:
                handleBorrowMainMenuUserInput(answer);
                break;
            case BORROW_LIST_ALL:
                handleBorrowListAllMenuUserInput(answer);
                break;
            case BORROW_SEARCH_BY_TITLE:
                // TODO implement
                break;
            case BORROW_SEARCH_BY_AUTHOR:
                // TODO implement
                break;
            case BORROW_SEARCH_BY_RELEASE_DATE:
                // TODO implement
                break;
            case DONATE:
                handleDonateMenuUserInput(answer);
                break;
            case RETURN:
                handleReturnMenuUserInput(answer);
                break;
            default:
                break;
        }
    }

    private static void renderMenu(boolean renderErrorMsg) {
        renderMainTitles();
        
        switch (menuType) {
            case MAIN:
                renderMainMenu();
                break;
            case BORROW_MAIN:
                renderBorrowMainMenu();
                break;
            case BORROW_LIST_ALL:
                renderBorrowListAllMenu();
                break;
            case BORROW_SEARCH_BY_TITLE:
                // TODO implement render method and logic
                break;
            case BORROW_SEARCH_BY_AUTHOR:
                // TODO implement render method and logic
                break;
            case BORROW_SEARCH_BY_RELEASE_DATE:
                // TODO implement render method and logic
                break;
            case DONATE:
                renderDonateMenu();
                break;
            case RETURN:
                renderReturnMenu();
                break;
            default:
                break;
        }

        System.out.println(ApplicationConstants.LINE_BREAK);

        if (renderErrorMsg) {
            System.err.println(ApplicationConstants.INVALID_INPUT);
            System.out.println(ApplicationConstants.LINE_BREAK);
            Application.renderErrorMsg = false;
        }
    }

    private static void renderMainMenu() {
        System.out.println("(1) " + ApplicationConstants.BORROW_BOOK);
        System.out.println("(2) " + ApplicationConstants.RETURN_BOOK);
        System.out.println("(3) " + ApplicationConstants.DONATE_BOOK);
        System.out.println("(4) " + ApplicationConstants.EXIT);
    }

    private static void renderBorrowMainMenu() {
        if (isBorrowSuccess) {
            System.out.println(ApplicationConstants.BORROW_SUCCESS);
            System.out.println(
                String.format("%s, %s, %s",
                    selectedBookToBorrow.getTitle(), 
                    selectedBookToBorrow.getReleaseDate(), 
                    selectedBookToBorrow.getAuthor())
            );
            System.out.println(ApplicationConstants.LINE_BREAK_SINGLE);
        }
        

        System.out.println("(1) " + ApplicationConstants.LIST_ALL);
        System.out.println("(2) " + ApplicationConstants.SEARCH_BY_TITLE);
        System.out.println("(3) " + ApplicationConstants.SEARCH_BY_AUTHOR);
        System.out.println("(4) " + ApplicationConstants.SEARCH_BY_RELEASE_DATE);
        System.out.println("(5) " + ApplicationConstants.BACK);
    }

    private static void renderBorrowListAllMenu() {
        System.out.println(ApplicationConstants.BORROW_SELECT_FROM_ALL);
        System.out.println(ApplicationConstants.LINE_BREAK_SINGLE);
        System.out.println(ApplicationConstants.LINE_BREAK);

        List<LibraryBook> allBook = library.getAllBook();
        for (LibraryBook book : allBook) {
            System.out.println(
                String.format("(%d) %s, %s, %s",
                    book.getId(),
                    book.getTitle(), 
                    book.getReleaseDate(), 
                    book.getAuthor())
            );
        }

        if (allBook.isEmpty()) {
            System.out.println(ApplicationConstants.NO_BORROWABLE_BOOK);
        }

        System.out.println(ApplicationConstants.LINE_BREAK);
        System.out.println("(1) " + ApplicationConstants.BACK);
    }

    private static void renderDonateMenu() {
        
    }

    private static void renderReturnMenu() {
        
    }

    private static void renderMainTitles() {
        clearConsole();
        if (menuType == MenuType.MAIN) {
            System.out.println(ApplicationConstants.WELCOME);
            System.out.println(ApplicationConstants.LINE_BREAK_DOUBLE);
        }

        if (isSecondaryTitleShouldBeRendered()) {
            System.out.println(ApplicationConstants.SELECT_FROM_THE_MENU);
            System.out.println(ApplicationConstants.LINE_BREAK_SINGLE);
            System.out.println(ApplicationConstants.LINE_BREAK);    
        }
    }

    private static boolean isSecondaryTitleShouldBeRendered() {
        return menuType == MenuType.MAIN
            || menuType == MenuType.BORROW_MAIN
            || menuType == MenuType.DONATE
            || menuType == MenuType.RETURN;
    }

    private static void handleMainMenuUserInput(String answer) throws InvalidUserInputException {
        int selectedMenu = tryParseInt(answer);
        if (selectedMenu < 1 || selectedMenu > ApplicationConstants.MAIN_MENU_LENGTH) {
            throw new InvalidUserInputException(ApplicationConstants.INVALID_INPUT);
        }

        switch (selectedMenu) {
            case 1:
                menuType = MenuType.BORROW_MAIN;
                break;
            case 2:
                
                break;
            case 3:
                
                break;    
            case 4:
                isApplicationRunning = false;
                break;            
            default:
                break;
        }
    }

    private static void handleBorrowMainMenuUserInput(String answer) throws InvalidUserInputException {
        int selectedMenu = tryParseInt(answer);
        if (selectedMenu < 1 || selectedMenu > ApplicationConstants.BORROW_MENU_LENGTH) {
            throw new InvalidUserInputException(ApplicationConstants.INVALID_INPUT);
        }

        switch (selectedMenu) {
            case 1:
                menuType = MenuType.BORROW_LIST_ALL;
                break;
            case 2:
                //menuType = MenuType.BORROW_SEARCH_BY_TITLE;
                break;
            case 3:
                //menuType = MenuType.BORROW_SEARCH_BY_AUTHOR;
                break;    
            case 4:
                //menuType = MenuType.BORROW_SEARCH_BY_RELEASE_DATE;
                break;            
            case 5:
                menuType = MenuType.MAIN;
                break;            
            default:
                break;
        }
    }

    private static void handleBorrowListAllMenuUserInput(String answer) throws InvalidUserInputException {
        if ("1".equals(answer) || selectedBookToBorrow == null) {
            isBorrowSuccess = false;
        } else {
            int selectedBookNumber = tryParseInt(answer);

            if (selectedBookNumber < 1 || selectedBookNumber > library.countBooks()) {
                throw new InvalidUserInputException(ApplicationConstants.INVALID_INPUT);
            }
            
            // TODO refactor
            library.getAllBook()
                .stream()
                .filter(book -> book.getId() == selectedBookNumber)
                .findFirst()
                .get()
                .setInTheLibrary(false);

            isBorrowSuccess = true;
        }

        menuType = MenuType.BORROW_MAIN;
    }

    private static void handleReturnMenuUserInput(String answer) {
        
    }

    private static void handleDonateMenuUserInput(String answer) {
        
    }

    private static Integer tryParseInt(String stringValue) throws InvalidUserInputException {
        try {
            return Integer.parseInt(stringValue);
        } catch (NumberFormatException e) {
            throw new InvalidUserInputException(ApplicationConstants.INVALID_INPUT);
        }
    }

    private static void clearConsole() {
        try {
            final String os = System.getProperty(ApplicationConstants.OS_NAME);

            if (os.contains(ApplicationConstants.OS_WINDOWS)) {
                Runtime.getRuntime().exec("cls");
            } else {
                System.out.println(ApplicationConstants.LINUX_CLEAR_TERMINAL);
                //Runtime.getRuntime().exec("clear");
            }
        } catch(Exception e) {
            // Catch error silently
            System.out.print("Console cannot be cleared");
        }
    }
}