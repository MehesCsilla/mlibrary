package tk.csupor.mlibrary.app;

public final class ApplicationConstants {
    // Global constants
    public static final String OS_NAME = "os.name";
    public static final String OS_WINDOWS = "Windows";
    public static final String LINE_BREAK_DOUBLE = "===========================";
    public static final String LINE_BREAK_SINGLE = "---------------------------";
    public static final String LINE_BREAK = "\n";
    public static final String LINUX_CLEAR_TERMINAL = "\033[H\033[2J";
    public static final int MAIN_MENU_LENGTH = 4;
    public static final int BORROW_MENU_LENGTH = 5;


    // Global texts
    public static final String WELCOME = "Welcome to Csilla's Library!";
    public static final String SELECT_FROM_THE_MENU = "Please select from the menu";
    public static final String BACK = "Go back";
    public static final String OK = "Ok";
    public static final String CANCEL = "Cancel";
    public static final String INVALID_INPUT = "Input is invlid. Please try an other one!";


    // Main menu texts
    public static final String BORROW_BOOK = "Borrow a book";
    public static final String RETURN_BOOK = "Return book";
    public static final String DONATE_BOOK = "Donate a book";
    public static final String EXIT = "Exit from the library";

    // Book borrowing texts
    public static final String LIST_ALL = "List all book";
    public static final String SEARCH_BY_TITLE = "Search for book by title";
    public static final String SEARCH_BY_AUTHOR = "Search for book by author";
    public static final String SEARCH_BY_RELEASE_DATE = "Search for book by release date";
    public static final String BORROW_SELECT_FROM_ALL = "Select from the books below";
    public static final String NO_BORROWABLE_BOOK = "Sorry, currently we don't have any borrowable book.\nPlease try it later!";
    public static final String BORROW_SUCCESS = "Book borrowed successfully:";

    // Book returning texts
    public static final String NO_SUCH_BOOK = "Sorry but there is no such book in the library. Maybe you want to donate?";
    public static final String RETURN_SUCCESSFUL = "Book is successfully returned";

    // Book returning & donating texts
    public static final String ADD_BOOK_TITLE = "Please type the title of the book";
    public static final String ADD_BOOK_AUTHOR = "Please type the author's name";
    public static final String ADD_BOOK_RELEASE_DATE = "Please type the release date of the book";

}