package tk.csupor.mlibrary.app;

public enum MenuType {
    MAIN,
    BORROW_MAIN,
    BORROW_LIST_ALL,
    BORROW_SEARCH_BY_TITLE,
    BORROW_SEARCH_BY_AUTHOR,
    BORROW_SEARCH_BY_RELEASE_DATE,
    RETURN,
    DONATE;
}