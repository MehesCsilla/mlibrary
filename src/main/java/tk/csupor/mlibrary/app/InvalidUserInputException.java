package tk.csupor.mlibrary.app;

public class InvalidUserInputException extends Exception {
    private static final long serialVersionUID = 3482254590874673768L;

    public InvalidUserInputException(String message) {
        super(message);
    }
}