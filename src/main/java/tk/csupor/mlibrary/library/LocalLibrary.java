package tk.csupor.mlibrary.library;

import java.util.*;

import tk.csupor.mlibrary.app.InvalidUserInputException;

public class LocalLibrary implements Library {

    private ArrayList<LibraryBook> library;

    public LocalLibrary() {
        this.library = new ArrayList<LibraryBook>();
        LibraryBook book1 = new LibraryBook(1, "Game of Thrones", "George R.R. Martin", 1996, "fantastic", false, true);
        LibraryBook book2 = new LibraryBook(2, "A Clash of Kings ", "George R.R. Martin", 1998, "fantastic", false,
                false);
        LibraryBook book3 = new LibraryBook(3, "A Storm of Swords", "George R.R. Martin", 2000, "fantastic", false,
                true);
        LibraryBook book4 = new LibraryBook(4, "A Feast for Crows", "George R.R. Martin", 2005, "fantastic", false,
                true);
        LibraryBook book5 = new LibraryBook(5, "A Dance with Dragons", "George R.R. Martin", 2011, "fantastic", false,
                true);
        LibraryBook book6 = new LibraryBook(6, "The Winds of Winter", "George R.R. Martin", 0, "fantastic", false,
                false);
        LibraryBook book7 = new LibraryBook(7, "A Dream of Spring ", "George R.R. Martin", 0, "fantastic", false, true);

        this.library.add(book1);
        this.library.add(book2);
        this.library.add(book3);
        this.library.add(book4);
        this.library.add(book5);
        this.library.add(book6);
        this.library.add(book7);
    }

    public List<LibraryBook> getAvaliableBooks() {
        List<LibraryBook> avaliableLibrary = new ArrayList<LibraryBook>();

        this.library.forEach(book -> {
            if (book.getInTheLibrary()) {
                avaliableLibrary.add(book);
            }
        });      

        return avaliableLibrary;
    }

    public List<LibraryBook> getUnavaliableBooks() {
        List<LibraryBook> unAvaliableLibrary = new ArrayList<LibraryBook>();

        this.library.forEach(book -> {
            if (book.getInTheLibrary() == false) {
                unAvaliableLibrary.add(book);
            }
        });       

        return unAvaliableLibrary;
    }

    @Override
    public void returnBook(LibraryBook returnBook) {
        for (LibraryBook book : library) {
            if (book.equals(returnBook)) {
                book.setInTheLibrary(true);
            }
        }
    }

    @Override
    public void donateBook(LibraryBook book) {
        book.setInTheLibrary(true);
        this.library.add(book);
        
    }

    public void clearTheLibrary() {
        this.library.clear();        
    }

    @Override
    public String toString() {
        return this.library.toString();
    }

    @Override
    public List<LibraryBook> getAllBook() {
        List<LibraryBook> allBook = getAvaliableBooks();
        List<LibraryBook> unavailableBooks = getUnavaliableBooks();
        allBook.addAll(unavailableBooks);
        Collections.sort(allBook);
        return allBook;
    }

    @Override
    public List<LibraryBook> findBookByTitle(String searchTitle) {
        List<LibraryBook> searchInTheLibrary = new ArrayList<LibraryBook>();

        for (LibraryBook book : library) {
            String title = book.getTitle();

            if (title.contains(searchTitle)) {
                searchInTheLibrary.add(book);
            }
        }

        Collections.sort(searchInTheLibrary);
        return searchInTheLibrary;
    }

    @Override
    public List<LibraryBook> findBooksByAuthor(String searchAuthor) {
        List<LibraryBook> searchInTheLibrary = new ArrayList<LibraryBook>();

        for (LibraryBook book : library) {
            String author = book.getAuthor();

            if (author.contains(searchAuthor)) {
                searchInTheLibrary.add(book);
            }
        }
        
        Collections.sort(searchInTheLibrary);
        return searchInTheLibrary;
    }

    @Override
    public List<LibraryBook> findBooksByReleaseDate(int searchReleaseDate) {
        List<LibraryBook> searchInTheLibrary = new ArrayList<LibraryBook>();

        for (LibraryBook book : library) {
            int releaseDate = book.getReleaseDate();

            if (releaseDate == searchReleaseDate) {
                searchInTheLibrary.add(book);
            }
        }
        
        Collections.sort(searchInTheLibrary);
        return searchInTheLibrary;
    }

    @Override
    public void borrowBook(LibraryBook borrowBook) throws InvalidUserInputException {
        for (LibraryBook book : library) {
            if (book.equals(borrowBook)) {
                book.setInTheLibrary(false);
            }
        }
    }

    @Override
    public int countBooks() {
        return library.size();
    }
}