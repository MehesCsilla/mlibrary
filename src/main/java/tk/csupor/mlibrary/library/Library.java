package tk.csupor.mlibrary.library;

import java.util.List;

import tk.csupor.mlibrary.app.InvalidUserInputException;

public interface Library {
    public List<LibraryBook> getAllBook();
    public List<LibraryBook> findBookByTitle(String title);
    public List<LibraryBook> findBooksByAuthor(String author);
    public List<LibraryBook> findBooksByReleaseDate(int year);
    public void borrowBook(LibraryBook book) throws InvalidUserInputException;
    public void returnBook(LibraryBook book) throws InvalidUserInputException;
    public void donateBook(LibraryBook book) throws InvalidUserInputException;
    public int countBooks();
}