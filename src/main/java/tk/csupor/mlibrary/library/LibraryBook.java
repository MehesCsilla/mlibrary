package tk.csupor.mlibrary.library;

import java.util.Comparator;

public class LibraryBook implements Comparable<LibraryBook>, Comparator<LibraryBook> {

    private int id;
    private String title;
    private String author;
    private int releaseDate;
    private String genre;
    private boolean forKids;
    private boolean inTheLibrary;

    LibraryBook(int id, String title, String author, int releaseDate, String genre, boolean forKids, boolean inTheLibrary) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.releaseDate = releaseDate;
        this.genre = genre;
        this.forKids = forKids;
        this.inTheLibrary = inTheLibrary;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public int getReleaseDate() {
        return releaseDate;
    }

    public String getGenre() {
        return genre;
    }

    public boolean getForKids() {
        return forKids;
    }

    public boolean getInTheLibrary() {
        return inTheLibrary;
    }

    public void setInTheLibrary(boolean inTheLibrary) {
        this.inTheLibrary = inTheLibrary;
    }


    @Override
    public int compare(LibraryBook book1, LibraryBook book2) {
        return book1.compareTo(book2);
    }

    @Override
    public int compareTo(LibraryBook book) {
        return this.title.compareTo(book.title);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        
        LibraryBook book = (LibraryBook) obj;
        
        return book.getId() == id
            && book.getTitle().equals(title)
            && book.getAuthor().equals(author)
            && book.getReleaseDate() == releaseDate
            && book.getGenre().equals(genre)
            && book.getForKids() == forKids;
    }

    /* Comparator for sorting the list by id */
    public static Comparator<LibraryBook> idComparator = new Comparator<LibraryBook>() {
        @Override
        public int compare(LibraryBook b1, LibraryBook b2) {
            int bookId1 = b1.getId();
            int bookId2 = b2.getId();
            return bookId1 - bookId2;
        }
    };

    /* Comparator for sorting the list by releasdate */
    public static Comparator<LibraryBook> releaseDateComparator = new Comparator<LibraryBook>() {
        @Override
        public int compare(LibraryBook b1, LibraryBook b2) {
            int bookId1 = b1.getReleaseDate();
            int bookId2 = b2.getReleaseDate();
            return bookId1 - bookId2;
        }
    };


    @Override
    public String toString() {
        return String.format(
                "\nBook: {\n  id: %d,\n  title: %s,\n  author: %s,\n  releaseDate: %s,\n  genre: %s,\n  forKids: %s,\n  inTheLibrary: %s\n}",
                this.id, this.title, this.author, this.releaseDate, this.genre, this.forKids, this.inTheLibrary);
    }
}