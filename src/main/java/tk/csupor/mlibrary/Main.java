package tk.csupor.mlibrary;

import tk.csupor.mlibrary.app.Application;

public class Main {
    public static void main(String[] args) {
        Application.run();
    }
}